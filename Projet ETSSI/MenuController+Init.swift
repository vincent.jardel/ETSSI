//
//  MenuController+Init.swift
//  Projet ETSSI
//
//  Created by Vincent Jardel on 22/11/2016.
//  Copyright © 2016 Vincent Jardel. All rights reserved.
//

import Foundation

protocol Revealable {
    weak var menuButton: UIBarButtonItem! { get set }
}

// Etend la classe UIViewController pour ne pas répéter le code dans le viewDidLoad de chaque UIViewController
extension UIViewController {
    func setupRevealMenu<T : UIViewController>(controller : T) where T : Revealable {
        if self.revealViewController() != nil {
            controller.menuButton.target = revealViewController()
            controller.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            
            revealViewController().rightViewRevealWidth = 150
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
}
