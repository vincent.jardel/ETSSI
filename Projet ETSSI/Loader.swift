//
//  Loader.swift
//  Projet ETSSI
//
//  Created by Vincent Jardel on 22/11/2016.
//  Copyright © 2016 Vincent Jardel. All rights reserved.
//

import Foundation

// Permet d'afficher l'Activity Indicator (loader)
class Loader {
    static func displayLoader(loader: UIActivityIndicatorView, view: UIView) -> Void {
        loader.center = view.center
        loader.hidesWhenStopped = true
        loader.startAnimating()
        
        view.addSubview(loader)
    }
}
