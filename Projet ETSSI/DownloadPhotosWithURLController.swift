//
//  DownloadImagesWithURL.swift
//  etssi
//
//  Created by Vincent Jardel on 22/11/2016.
//  Copyright © 2016 Vincent Jardel. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyJSON

class DownloadImagesWithURL: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, Revealable {
    
    @IBOutlet var colView: UICollectionView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var timer = Date()
    var URLArray = [[String:AnyObject]]()
    let loader = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Initalise le menu et afficher le loader le temps du chargement de la BDD
        setupRevealMenu(controller: self)
        Loader.displayLoader(loader: self.loader, view: self.view)
    }
    
    // Récupère et stocke les photos en JSON dans un array.
    override func viewWillAppear(_ animated: Bool) {
        let apiURL = "https://api.unsplash.com/photos/?client_id=71ad401443f49f22556bb6a31c09d62429323491356d2e829b23f8958fd108c4"
        Alamofire.request(apiURL).responseJSON { (responseData) -> Void in
            
            // Si il y a une réponse de l'URL, on stocke l'URL des images dans un array
            if ((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                if let resData = swiftyJsonVar.arrayObject {
                    self.URLArray = resData as! [[String:AnyObject]]
                }
                
                if self.URLArray.count > 0 {
                    self.colView.reloadData()
                    self.loader.stopAnimating()
                }
            }
        }
    }
    
    // MARK: - Collection View
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.URLArray.count
    }
    
    // Affiche les images grâce à l'URL récupéré grâce à l'API en taille Small
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImageCell
        var dict = URLArray[(indexPath as NSIndexPath).row]
        let url = URL(string: dict["urls"]?["small"] as! String)!
        cell.img.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: { image in
            // Affiche le nombre de secondes pour afficher la cellule
            let methodFinish = NSDate()
            let executionTime = methodFinish.timeIntervalSince(self.timer as Date)
            print("Download: \(executionTime)")
        })
        
        return cell
    }
    
    // Permet de gèrer la taille des cellules
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewSize = view.bounds.size
        let spacing: CGFloat = 0.5
        let width = (viewSize.width / 2) - spacing
        let height = (viewSize.width / 3) - spacing
        return CGSize(width: width, height: height)
    }
}

