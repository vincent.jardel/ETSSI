//
//  SwipeController.swift
//  Projet ETSSI
//
//  Created by Vincent Jardel on 06/02/2017.
//  Copyright © 2017 Vincent Jardel. All rights reserved.
//

import UIKit

class SwipeController: UIViewController, UITableViewDataSource, UITableViewDelegate, Revealable {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Initalise le menu
        setupRevealMenu(controller: self)
    }
    
    // MARK: - Collection View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    // Donne un titre aux cellules avec le numéro de la cellule
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = "Example \(indexPath.row + 1)"
        
        return cell
    }
    
    // Permet le swipe sur les cellules
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Crée différents button au swipe
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        let more = UITableViewRowAction(style: .normal, title: "Plus") { action, index in
            print("Bouton plus")
        }
        more.backgroundColor = .lightGray
        
        let favorite = UITableViewRowAction(style: .normal, title: "Favoris") { action, index in
            print("Bouton favoris")
        }
        favorite.backgroundColor = .orange
        
        let share = UITableViewRowAction(style: .normal, title: "Partager") { action, index in
            print("Bouton partage")
        }
        share.backgroundColor = .blue
        
        return [share, favorite, more]
    }
}
