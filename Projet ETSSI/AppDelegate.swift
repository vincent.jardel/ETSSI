//
//  AppDelegate.swift
//  Projet ETSSI
//
//  Created by Vincent Jardel on 23/11/2016.
//  Copyright © 2016 Vincent Jardel. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    // Initalise FireBase grâce aux informations dans le .plist
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FIRApp.configure()
        return true
    }
}

