//
//  ImageCell.swift
//  Projet ETSSI
//
//  Created by Vincent Jardel on 23/11/2016.
//  Copyright © 2016 Vincent Jardel. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
}
