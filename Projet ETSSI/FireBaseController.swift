//
//  FireBaseController.swift
//  Projet ETSSI
//
//  Created by Vincent Jardel on 04/01/2017.
//  Copyright © 2017 Vincent Jardel. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FireBaseController: UIViewController, UITableViewDataSource, UITableViewDelegate, Revealable {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    var ref: FIRDatabaseReference!
    var nameArray = [String]()
    var timer = Date()
    let loader = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Initalise le menu et afficher le loader le temps du chargement de la BDD
        setupRevealMenu(controller: self)
        Loader.displayLoader(loader: self.loader, view: self.view)
        
        ref = FIRDatabase.database().reference()
        // Entre dans l'arborescence (users) du JSON recueilli grâce à FireBase
        ref.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    // Recueille les éléments dont on a besoin : email, name et on concatène
                    let email = snap.childSnapshot(forPath: "email").value! as! String
                    let name = snap.childSnapshot(forPath: "name").value! as! String
                    let nameWithEmail = "\(name) : \(email)"
                    self.nameArray.append(nameWithEmail)
                }
                
                // Une fois les données chargées, on retire l'activity indicator et on rafraîchit la tableView
                self.loader.stopAnimating()
                self.tableView.reloadData()
            }
        })
    }
    
    // MARK: - Collection View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let methodFinish = NSDate()
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        // Affiche le nom des personnes
        cell.textLabel?.text = self.nameArray[indexPath.row]
        let executionTime = methodFinish.timeIntervalSince(self.timer as Date)
        print("FireBase: \(executionTime)")
        
        return cell
    }
    
    // Placeholder dans le header de la tableView
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Nom : Email"
    }
    
    // Design headerView de la tableView
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.white
        let border = UIView(frame: CGRect(x: 0, y: header.contentView.bounds.size.height, width: self.view.bounds.width, height: 0.5))
        border.backgroundColor = self.tableView.separatorColor
        header.addSubview(border)
        header.textLabel?.textColor = self.tableView.separatorColor
    }
}
